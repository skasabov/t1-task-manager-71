package ru.t1.skasabov.tm.integration.rest;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.client.rest.AuthRestEndpointClient;
import ru.t1.skasabov.tm.client.rest.ProjectRestEndpointClient;
import ru.t1.skasabov.tm.client.rest.TaskRestEndpointClient;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectRestEndpointClientTest {

    private static final int NUMBER_OF_ENTITIES = 4;

    @NotNull
    private final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    private final TaskRestEndpointClient taskClient = TaskRestEndpointClient.client();

    @NotNull
    private final AuthRestEndpointClient authClient = AuthRestEndpointClient.client();

    @NotNull
    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    @NotNull
    private final ProjectDto project3 = new ProjectDto("Test Project 3");

    @NotNull
    private final ProjectDto project4 = new ProjectDto("Test Project 4");

    private boolean isAuth = false;

    @NotNull
    private String userId;

    @NotNull
    private List<ProjectDto> projectList = new ArrayList<>();

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    @Before
    public void initTest() {
        Assert.assertTrue(authClient.login("test", "test").isSuccess());
        @Nullable final UserDto user = authClient.profile();
        Assert.assertNotNull(user);
        userId = user.getId();
        @Nullable final List<ProjectDto> projects = client.findAll();
        projectList = projects == null ? Collections.emptyList() : projects;
        @Nullable final List<TaskDto> tasks = taskClient.findAll();
        taskList = tasks == null ? Collections.emptyList() : tasks;
        client.clear();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        project4.setUserId(userId);
        client.save(project1);
        client.save(project2);
        client.save(project3);
        client.save(project4);
        @NotNull final TaskDto task1 = new TaskDto("Test Task 1");
        task1.setProjectId(project1.getId());
        task1.setUserId(userId);
        @NotNull final TaskDto task2 = new TaskDto("Test Task 2");
        task2.setProjectId(project2.getId());
        task2.setUserId(userId);
        @NotNull final TaskDto task3 = new TaskDto("Test Task 3");
        task3.setProjectId(project3.getId());
        task3.setUserId(userId);
        @NotNull final TaskDto task4 = new TaskDto("Test Task 4");
        task4.setProjectId(project4.getId());
        task4.setUserId(userId);
        taskClient.save(task1);
        taskClient.save(task2);
        taskClient.save(task3);
        taskClient.save(task4);
    }

    @After
    public void clean() {
        if (isAuth) {
            Assert.assertTrue(authClient.login("test", "test").isSuccess());
        }
        isAuth = false;
        client.clear();
        for (@NotNull final TaskDto task : taskList) taskClient.save(task);
        for (@NotNull final ProjectDto project : projectList) client.save(project);
        authClient.logout();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllTest() {
        @Nullable final List<ProjectDto> projects = client.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(NUMBER_OF_ENTITIES, projects.size());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void findAllNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.findAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveTest() {
        @NotNull final ProjectDto project = new ProjectDto("Test Project");
        project.setUserId(userId);
        client.save(project);
        Assert.assertEquals(NUMBER_OF_ENTITIES + 1, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void saveNoAuthTest() {
        isAuth = true;
        authClient.logout();
        @NotNull final ProjectDto project = new ProjectDto("Test Project");
        project.setUserId(userId);
        client.save(project);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByIdTest() {
        @Nullable final ProjectDto project = client.findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals("Test Project 1", project.getName());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void findByEmptyIdTest() {
        client.findById("");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByInvalidIdTest() {
        Assert.assertNull(client.findById("123"));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void findByIdNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.findById(project1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByIdTest() {
        Assert.assertTrue(client.existsById(project1.getId()));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void existsByEmptyIdTest() {
        client.existsById("");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByInvalidIdTest() {
        Assert.assertFalse(client.existsById("123"));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void existsByIdNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.existsById(project1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void countTest() {
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void countNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.count();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByIdTest() {
        client.deleteById(project1.getId());
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void deleteByEmptyIdTest() {
        client.deleteById("");
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByInvalidIdTest() {
        client.deleteById("some_id");
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void deleteByIdNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.deleteById(project1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteTest() {
        client.delete(project1);
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void deleteNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.delete(project1);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteProjectsTest() {
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(project2);
        projects.add(project3);
        projects.add(project4);
        client.deleteAll(projects);
        Assert.assertEquals(NUMBER_OF_ENTITIES - projects.size(), client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteEmptyProjectsTest() {
        client.deleteAll(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = IllegalArgumentException.class)
    public void deleteNullProjectsTest() {
        client.deleteAll(null);
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void deleteProjectsNoAuthTest() {
        isAuth = true;
        authClient.logout();
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        projects.add(project2);
        projects.add(project3);
        projects.add(project4);
        client.deleteAll(projects);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void clearTest() {
        client.clear();
        Assert.assertEquals(0, client.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = FeignException.class)
    public void clearNoAuthTest() {
        isAuth = true;
        authClient.logout();
        client.clear();
    }

}
