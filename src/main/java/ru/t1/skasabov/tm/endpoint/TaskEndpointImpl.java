package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.ITaskEndpoint;

import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.skasabov.tm.api.ITaskEndpoint")
public final class TaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @Nullable
    @Override
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskRepository.findAllByUserId(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDto save(
            @NotNull
            @RequestBody
            @WebParam(name = "task", partName = "task")
            final TaskDto task
    ) {
        task.setUserId(UserUtil.getUserId());
        return taskRepository.save(task);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull
            @PathVariable("id")
            @WebParam(name = "id", partName = "id")
            final String id
    ) {
        return taskRepository.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @PathVariable("id")
            @WebParam(name = "id", partName = "id")
            final String id
    ) {
        return taskRepository.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskRepository.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @PathVariable("id")
            @WebParam(name = "id", partName = "id")
            final String id
    ) {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @RequestBody
            @WebParam(name = "task", partName = "task")
            final TaskDto task
    ) {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task.getId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @Nullable
            @RequestBody
            @WebParam(name = "tasks", partName = "tasks")
            final List<TaskDto> tasks
    ) {
        if (tasks == null) return;
        for (@NotNull final TaskDto task : tasks) {
            taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task.getId());
        }
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
    }

}
